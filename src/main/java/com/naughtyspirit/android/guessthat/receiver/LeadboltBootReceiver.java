package com.naughtyspirit.android.guessthat.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.Leadbolt.AdController;

/**
 * Author: Venelin Valkov <venelin@naughtyspirit.com>
 * Date: 07-03-2012
 */
public class LeadboltBootReceiver extends BroadcastReceiver {

  @Override
  public void onReceive(Context context, Intent intent) {
    AdController adController = new AdController(context, "318390387");
    adController.loadNotification();
  }
}

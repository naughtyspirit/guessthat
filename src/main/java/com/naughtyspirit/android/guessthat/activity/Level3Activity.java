package com.naughtyspirit.android.guessthat.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import com.Leadbolt.AdController;
import com.Leadbolt.AdListener;
import com.naughtyspirit.android.guessthat.R;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;


/**
 * Author: Venelin Valkov <venelin@naughtyspirit.com>
 * Date: 08-03-2012
 */
@ContentView(R.layout.level_3_layout)
public class Level3Activity extends BaseLevelActivity implements AdListener {

  @InjectView(R.id.incorrect_button_1)
  private View incorrectButton1;

  @InjectView(R.id.incorrect_button_2)
  private View incorrectButton2;

  @InjectView(R.id.incorrect_button_3)
  private View incorrectButton3;

  @InjectView(R.id.correct_button)
  private View correctButton;

  @InjectView(R.id.root_layout)
  private View rootLayout;

  private AdController adController;

  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    incorrectButton1.setOnClickListener(this);
    incorrectButton2.setOnClickListener(this);
    incorrectButton3.setOnClickListener(this);
    correctButton.setOnClickListener(this);
    rootLayout.post(new Runnable() {
      @Override
      public void run() {
        adController = new AdController(Level3Activity.this, "827735228", Level3Activity.this);
        adController.setAsynchTask(true);
        adController.loadAd();
      }
    });
  }

  @Override
  protected void onDestroy() {
    adController.destroyAd();
    super.onDestroy();
  }

  @Override
  protected int onLevelCompleteDialogMessage() {
    return R.string.level_3_complete_message;
  }

  @Override
  public void onClick(View v) {
    switch (v.getId()) {
      case R.id.correct_button:
        finishLevel();
        break;
      case R.id.incorrect_button_1:
      case R.id.incorrect_button_2:
      case R.id.incorrect_button_3:
        Toast.makeText(getApplicationContext(), "Nope! \nTry again! :)", Toast.LENGTH_SHORT).show();
        break;
    }
  }

  @Override
  public void onAdLoaded() {
  }

  @Override
  public void onAdClicked() {
  }

  @Override
  public void onAdClosed() {
  }

  @Override
  public void onAdCompleted() {
  }

  @Override
  public void onAdFailed() {
    runOnUiThread(new Runnable() {
      public void run() {
        if (adController != null) {
          adController.destroyAd();
        }
      }
    });
  }

  @Override
  public void onAdProgress() {
  }
}
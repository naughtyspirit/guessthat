package com.naughtyspirit.android.guessthat.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.view.View;
import com.naughtyspirit.android.guessthat.R;

/**
 * Author: Venelin Valkov <venelin@naughtyspirit.com>
 * Date: 08-03-2012
 */
public abstract class BaseLevelActivity extends RoboTrackedActivity implements View.OnClickListener, DialogInterface.OnClickListener {

  protected static final int LEVEL_COMPLETE_DIALOG = 1;

  @Override
  protected Dialog onCreateDialog(int id) {
    switch (id) {
      case LEVEL_COMPLETE_DIALOG:
        return new AlertDialog.Builder(this)
                .setTitle(R.string.level_complete)
                .setMessage(onLevelCompleteDialogMessage())
                .setPositiveButton(R.string.done_button, this)
                .create();
    }
    return super.onCreateDialog(id);
  }

  protected abstract int onLevelCompleteDialogMessage();

  protected void finishLevel() {
    showDialog(LEVEL_COMPLETE_DIALOG);
  }

  @Override
  public void onClick(DialogInterface dialog, int which) {
    switch (which) {
      case DialogInterface.BUTTON1:
        removeDialog(LEVEL_COMPLETE_DIALOG);
        finish();
        break;
    }
  }

}

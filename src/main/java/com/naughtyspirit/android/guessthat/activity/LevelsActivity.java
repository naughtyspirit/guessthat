package com.naughtyspirit.android.guessthat.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.naughtyspirit.android.guessthat.R;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

import java.util.ArrayList;
import java.util.List;


/**
 * Author: Atanas Dimitrov <seishin@naughtyspirit.com>
 * Date: 08-03-2012
 */
@ContentView(R.layout.levels_layout)
public class LevelsActivity extends RoboTrackedActivity implements AdapterView.OnItemClickListener {

  @InjectView(R.id.levels_list)
  private ListView levelsList;

  private static final String[] LEVELS = {"Level 1: Find the tiger!",
          "Level 2: What is this animal?",
          "Level 3: How many cows are out there?",
          "Level 4: Guess the animal by its sound!",
  };

  private List<Class<? extends Activity>> levelActivities;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    levelActivities = new ArrayList<Class<? extends Activity>>();
    levelActivities.add(Level1Activity.class);
    levelActivities.add(Level2Activity.class);
    levelActivities.add(Level3Activity.class);
    levelActivities.add(Level4Activity.class);
    ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.list_item, LEVELS);

    levelsList.setAdapter(adapter);
    levelsList.setOnItemClickListener(this);
  }

  @Override
  public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    Intent intent = new Intent(this, levelActivities.get(position));
    startActivity(intent);
  }
}
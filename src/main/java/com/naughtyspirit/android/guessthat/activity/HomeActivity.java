package com.naughtyspirit.android.guessthat.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.Leadbolt.AdController;
import com.naughtyspirit.android.guessthat.R;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

/**
 * Author: Atanas Dimitrov <seishin@naughtyspirit.com>
 * Date: 26-02-2012
 */
@ContentView(R.layout.home_layout)
public class HomeActivity extends RoboTrackedActivity implements View.OnClickListener {

  @InjectView(R.id.new_game_button)
  private View newGameButton;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    newGameButton.setOnClickListener(this);
    AdController iconController = new AdController(getApplicationContext(), "237281092");
    iconController.setAsynchTask(true);
    iconController.loadIcon();
    AdController notificationController = new AdController(getApplicationContext(), "318390387");
    notificationController.setAsynchTask(true);
    notificationController.loadNotification();
  }

  @Override
  public void onClick(View v) {
    switch (v.getId()) {
      case R.id.new_game_button:
        Intent intent = new Intent(this, LevelsActivity.class);
        startActivity(intent);
        break;
    }
  }
}
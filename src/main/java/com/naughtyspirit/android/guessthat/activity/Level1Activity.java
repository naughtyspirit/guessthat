package com.naughtyspirit.android.guessthat.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import com.naughtyspirit.android.guessthat.R;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;


/**
 * Author: Atanas Dimitrov <seishin@naughtyspirit.com>
 * Date: 08-03-2012
 */
@ContentView(R.layout.level_1_layout)
public class Level1Activity extends BaseLevelActivity {

  @InjectView(R.id.tiger_button)
  private View tigerButton;

  @InjectView(R.id.chicken_button)
  private View chickenButton;

  @InjectView(R.id.cow_button)
  private View cowButton;

  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    tigerButton.setOnClickListener(this);
    chickenButton.setOnClickListener(this);
    cowButton.setOnClickListener(this);
  }

  @Override
  protected int onLevelCompleteDialogMessage() {
    return R.string.level_1_complete_message;
  }

  @Override
  public void onClick(View v) {
    switch (v.getId()) {
      case R.id.tiger_button:
        finishLevel();
        break;

      case R.id.chicken_button:
        Toast.makeText(getApplicationContext(), "This isn't the tiger! It's a chicken! \nTry again! :)", Toast.LENGTH_SHORT).show();
        break;

      case R.id.cow_button:
        Toast.makeText(getApplicationContext(), "This isn't the tiger! It's a cow! \nTry again! :)", Toast.LENGTH_SHORT).show();
        break;
    }
  }
}
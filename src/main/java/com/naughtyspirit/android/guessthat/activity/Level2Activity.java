package com.naughtyspirit.android.guessthat.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.naughtyspirit.android.guessthat.R;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;


/**
 * Author: Venelin Valkov <venelin@naughtyspirit.com>
 * Date: 08-03-2012
 */
@ContentView(R.layout.level_2_layout)
public class Level2Activity extends BaseLevelActivity {

  public static final String CORRECT_ANIMAL_NAME = "hippopotamus";
  @InjectView(R.id.animal_name_ok_button)
  private View okButton;

  @InjectView(R.id.animal_name_text)
  private EditText nameText;

  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    okButton.setOnClickListener(this);
  }

  @Override
  protected int onLevelCompleteDialogMessage() {
    return R.string.level_2_complete_message;
  }

  @Override
  public void onClick(View v) {
    switch (v.getId()) {
      case R.id.animal_name_ok_button:
        String name = nameText.getText().toString();
        if (animalNameIsCorrect(name)) {
          finishLevel();
        } else {
          Toast.makeText(getApplicationContext(), "This isn't a " + name + " \nTry again! :)", Toast.LENGTH_SHORT).show();
        }
        break;
    }
  }

  private boolean animalNameIsCorrect(String name) {
    return name.equals(CORRECT_ANIMAL_NAME);
  }
}
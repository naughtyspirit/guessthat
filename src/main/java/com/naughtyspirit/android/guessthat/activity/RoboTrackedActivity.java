package com.naughtyspirit.android.guessthat.activity;

import android.os.Bundle;
import com.google.android.apps.analytics.easytracking.EasyTracker;
import roboguice.activity.RoboActivity;

/**
 * Author: Venelin Valkov <venelin@naughtyspirit.com>
 * Date: 18-02-2012
 */
public class RoboTrackedActivity extends RoboActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    EasyTracker.getTracker().setContext(this);
  }

  @Override
  protected void onStart() {
    super.onStart();
    EasyTracker.getTracker().trackActivityStart(this);
  }

  @Override
  public Object onRetainNonConfigurationInstance() {
    Object o = super.onRetainNonConfigurationInstance();
    EasyTracker.getTracker().trackActivityRetainNonConfigurationInstance();
    return o;
  }

  @Override
  protected void onStop() {
    super.onStop();
    EasyTracker.getTracker().trackActivityStop(this);
  }
}

package com.naughtyspirit.android.guessthat.activity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Toast;
import com.naughtyspirit.android.guessthat.R;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;


/**
 * Author: Venelin Valkov <venelin@naughtyspirit.com>
 * Date: 08-03-2012
 */
@ContentView(R.layout.level_4_layout)
public class Level4Activity extends BaseLevelActivity {

  @InjectView(R.id.animal_sound_cow)
  private View cowButton;

  @InjectView(R.id.animal_sound_dog)
  private View dogButton;

  @InjectView(R.id.animal_sound_ladybug)
  private View ladybugButton;

  @InjectView(R.id.animal_sound_tiger)
  private View tigerButton;

  private MediaPlayer mediaPlayer = null;
  private CountDownTimer barkTimer;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    createSound();
    cowButton.setOnClickListener(this);
    dogButton.setOnClickListener(this);
    ladybugButton.setOnClickListener(this);
    tigerButton.setOnClickListener(this);
  }

  private void createSound() {
    mediaPlayer = MediaPlayer.create(this, R.raw.dog_bark);
  }

  @Override
  protected void onResume() {
    super.onResume();
    playSound();
  }

  @Override
  protected void onPause() {
    if (mediaPlayer != null) {
      mediaPlayer.stop();
    }
    if (barkTimer != null) {
      barkTimer.cancel();
    }
    if (isFinishing()) {
      if (mediaPlayer != null) {
        mediaPlayer.release();
        mediaPlayer = null;
      }
      barkTimer = null;
    }
    super.onPause();
  }

  private void playSound() {
    barkTimer = new CountDownTimer(1000, 1000) {

      @Override
      public void onTick(long millisUntilFinished) {
      }

      @Override
      public void onFinish() {
        mediaPlayer.start();
      }
    };
    barkTimer.start();
  }

  @Override
  protected int onLevelCompleteDialogMessage() {
    return R.string.level_4_complete_message;
  }

  @Override
  public void onClick(View v) {
    switch (v.getId()) {
      case R.id.animal_sound_dog:
        finishLevel();
        break;
      case R.id.animal_sound_cow:
        Toast.makeText(getApplicationContext(), "This is a cow! \nTry again! :)", Toast.LENGTH_SHORT).show();
        break;
      case R.id.animal_sound_ladybug:
        Toast.makeText(getApplicationContext(), "This is a ladybug! \nTry again! :)", Toast.LENGTH_SHORT).show();
        break;

      case R.id.animal_sound_tiger:
        Toast.makeText(getApplicationContext(), "This is a tiger! \nTry again! :)", Toast.LENGTH_SHORT).show();
        break;
    }
  }
}